var trustUsFeedback = function trustUsFeedback() {
    var self = this;

    this.items = Array.from(document.querySelectorAll('.trust-us__icons-item'));
    this.itemsWrapper = document.getElementsByClassName('trust-us__icons')[0];
    this.round = document.getElementById('js-logo-wrapper');
    this.backfeeds = Array.from(document.querySelectorAll('.audio-backfeed'));

    this.moveRound(this.items[0]);
    this.changeSlide(0);

    this.items.map(function (item, index) {
        item.onclick = function () {
            pauseAllPlayers();
            self.moveRound(item);
            self.changeSlide(index);
        };
    });
};

trustUsFeedback.prototype.moveRound = function(element){
    var logoRect = element.getBoundingClientRect();
    var containerRect = this.itemsWrapper.getBoundingClientRect();
    var pos = {
        left: logoRect.left - containerRect.left,
        top: logoRect.top - containerRect.top
    }    
    this.round.style.left = pos.left + 'px';
    this.round.style.top  = pos.top + 'px';
}

trustUsFeedback.prototype.changeSlide = function (item_index) {
    this.backfeeds.map(function (item, index) {
        if (item_index === index) {
            item.classList.remove('is_hidden');
        } else {
            item.classList.add('is_hidden');
        }
    });
};
