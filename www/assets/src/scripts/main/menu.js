function onloadedMenu() {
    updateMenu();

    setTimeout(function(){
        /* Делаем каждому элементу меню разную задержку при переходах */
        var menuItems = document.querySelectorAll('.menu__item');
        for (var i = menuItems.length - 1; i >= 0; i--) {
            menuItems[i].style.transitionDelay = i * 0.2 + 's';
        }

        var menuLinks = document.querySelectorAll('.menu__link');
        for (var i = menuLinks.length - 1; i >= 0; i--) {
            var link = menuLinks[i];

            if ( link.host     === window.location.host
              && link.pathname === window.location.pathname
              && link.hash     === ''
             ) {
                link.hash = "#top";
                if (link.host === window.location.host && link.pathname === '/') {
                    link.hash = "#home";
                }
            }

            link.addEventListener('click', closeMenu);
            link.addEventListener('click', updateMenu);
        }
    }, 1);
}

function updateMenu() {
    setTimeout(function(){
        var host = window.location.host;
        var pathname = window.location.pathname;
        var hash = window.location.hash;
        var menuLinks = document.querySelectorAll('.menu__link');
        for (var i = menuLinks.length - 1; i >= 0; i--) {
            if (menuLinks[i].host + menuLinks[i].pathname + menuLinks[i].hash === host + pathname + hash) {
                menuLinks[i].classList.add('active');
            } else {
                menuLinks[i].classList.remove('active');
            }
        }
    }, 0);
}


function openMenu() {
    document.documentElement.classList.add('menu--open');
}

function closeMenu() {
    document.documentElement.classList.remove('menu--open');
}
