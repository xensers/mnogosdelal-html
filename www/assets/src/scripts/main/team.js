document.querySelectorAll('.dorofeevtsi-list__item').forEach(function(item) {
  var interval = null;
  var img = item.querySelector('img');

  item.onmouseover = function() {
    interval = setInterval(requestAnimationFrame, 80,function () {
      var x = Math.random() * 5 - 2.5;
      var y = Math.random() * 5 - 2.5;
      var z = Math.random() * 5 - 2.5;

      img.style.transform = 'translate(' + x + 'px,' + y  + 'px) rotate('+ z +'deg) scale(1.1)';
    });
  };

  item.onmouseout = function () {
    clearInterval(interval);
    img.style.transform = '';
  }
});


$(document).ready(function() {
  $(".team__video").lightGallery({
    selector: '.team__preview',
    download: false
  }); 

  $(".gallery").lightGallery({
    selector: '.gallery__link',
    download: false,
    thumbnail: false,
    addClass: 'team__galleris'
  }); 
});