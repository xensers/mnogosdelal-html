function feedback() {
  var elemBtn = document.querySelector('.feedback-form__btn');
  var elemModal = document.querySelector('.feedback-modal');
  var elemStiker = elemModal.querySelector('.stiker');
  var elemSuccessfulSend = elemModal.querySelector('.feedback-modal__successful-send');
  var objStikers = new Stikers();

  var drawStiker = function (progress) {
    objStikers.drawToRight(elemStiker, progress);
  }

  inputsHandler();
  elemModal.onclick = modalClose;

  $(document).on('af_complete', function(event, response) {
      console.log(response);
      if (response.success) {
        modalOpen();
      }
  });

  function inputsHandler() {
      var inputs = Array.from(document.querySelectorAll('.feedback-form__input'));

      inputs.map(function (item) {
          item.onblur = function (e) {
              if (e.target.value.trim()) {
                  item.classList.add('is_fill');
              } else {
                  item.classList.remove('is_fill');
                  item.value = "";
              }
          };
      });
  };


  function modalOpen() {
    document.documentElement.classList.add('hidefixed');
    elemModal.style.display = 'block';
    drawStiker(1);

    successfulSend();

    return false;
  }

  function modalClose() {
    animate({
      draw: drawStiker,
      from: 1,
      to: 100,
      duration: 500,
    });

    animate({
      duration: 500,
      draw: function(progress) {
        elemModal.style.opacity = 1 - progress;
      },
      after: function() {
         resetStyle();
         document.documentElement.classList.remove('hidefixed');
      },
    });

    return false;
  }

  function successfulSend() {
    elemSuccessfulSend.style.display = 'block';
    animate({
      draw: drawStiker,
      from: 100,
      to: 1,
      duration: 500,
    });
  }

  function resetStyle() {
    elemModal.removeAttribute('style');
    elemSuccessfulSend.removeAttribute('style');
  }
}
