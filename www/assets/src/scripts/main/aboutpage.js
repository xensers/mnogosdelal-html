// Slider 
function initSlider() {
    var mySwiper = new Swiper('.history-slider', {
        pagination: {
            el: '.history-slider__pagination'
        },
        navigation: {
            nextEl: '.history-slider__next',
            prevEl: '.history-slider__prev'
        },
        effect: 'coverflow',
        spaceBetween: 20,
        speed: 500,
        breakpointsInverse: true,
        breakpoints: {
            1920: {
                spaceBetween: 120
            },
            3996: {
                spaceBetween: 950
            }

        }
    });
}

function firstSectionAnimation() {
    var elemParagraphs = document.querySelectorAll('.first-section__content p');

    var delay = 2.5;
    for (var i = 0; i <= elemParagraphs.length - 1; i++) {
        elemParagraphs[i].style.animationDelay = delay + 's';
        delay = delay + 0.4;
    }

    setTimeout(function(){
        var iteration = 0;
        var elem = document.getElementsByClassName('first-section__figure-img')[0];

        var interval = setInterval(function(){
            requestAnimationFrame(function(){
                if (elem.style.transform) {
                    elem.style.transform = '';
                } else {
                    elem.style.transform = 'scale(-1, 1)';
                }
                iteration++;
                if (iteration >= 4) {
                    clearInterval(interval);
                }
            });
        }, 1000);
    }, 2000);
}
