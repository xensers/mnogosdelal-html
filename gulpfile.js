const { task, watch, series, parallel, src, dest } = require('gulp'),
      browserSync = require('browser-sync').create();

const rename = require('gulp-rename'),
      cache  = require('gulp-cache'),
      del    = require('del'),
      named  = require('vinyl-named');

const sass         = require('gulp-sass'),
      aliases      = require('gulp-style-aliases'),
      sourcemaps   = require('gulp-sourcemaps'),
      cssnano      = require('gulp-cssnano'),
      autoprefixer = require('gulp-autoprefixer');

const imagemin    = require('gulp-imagemin'),
      imgCompress = require('imagemin-jpeg-recompress');

const plumber = require('gulp-plumber'),
      notify  = require('gulp-notify');

const
      // webpack = require('webpack'),
      concat        = require('gulp-concat'),
      // webpackStream = require('webpack-stream'),
      // webpackConfig = require('./webpack.config.js'),
      uglify        = require('gulp-uglify');

const errorHandler = notify.onError('<%= error.message %>');

let paths = {
  assets : 'www/assets/',
  src    : 'www/assets/src/',
  dist   : 'www/assets/dist/'
};

paths = {
  ...paths,
  styles : {
    src:   paths.src+'scss/*.scss',
    dest:  paths.dist+'css/',
    watch: paths.src+'scss/**/*',
  },
  scripts : {
    src:  paths.src+'scripts/*.js',
    dest: paths.dist+'scripts/',
    watch: paths.src+'scripts/**/*'
  },
  images : {
    src:   paths.src+'images/**/*',
    dest:  paths.dist+'images/',
    watch: 'src'
  },
  fonts : {
    src: paths.src+'fonts/**/*',
    dest: paths.dist+'fonts/',
    watch: 'src'
  }
};

task('serve', function(done) {
  browserSync.init({
    server: {
      baseDir: "www/"
    }
  });

  browserSync.watch('www/').on('change', browserSync.reload);

  done();
});

task('clean', done => del(paths.dist));
task('clear', done => cache.clearAll());

task('scripts', done => {
  src(paths.scripts.src)
    .pipe(plumber({errorHandler}))
    .pipe(uglify())
    .pipe(dest(paths.scripts.dest))
    .pipe(browserSync.reload({stream: true}));

  src(paths.src+'scripts/main/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(dest(paths.scripts.dest))
    .pipe(browserSync.reload({stream: true}));

  done();
});

task('styles', done => src(paths.styles.src)
    .pipe(plumber({errorHandler}))
    .pipe(aliases({
      "~": "./node_modules/",
      "@": paths.src
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(autoprefixer())
    .pipe(dest(paths.styles.dest))
    .pipe(cssnano({
        reduceIdents: false
     }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest(paths.styles.dest))
    .pipe(browserSync.reload({stream: true}))
);

task('images', done => src(paths.images.src)
  .pipe(cache(imagemin([
    imgCompress({
      loops: 4,
      min: 70,
      max: 80,
      quality: 'high'
    }),
    imagemin.gifsicle(),
    imagemin.optipng(),
    imagemin.svgo()
   ])))
  .pipe(dest(paths.images.dest))
  .pipe(browserSync.reload({stream: true}))
);

task('fonts', done => src(paths.fonts.src)
  .pipe(plumber({errorHandler}))
  .pipe(dest(paths.fonts.dest))
  .pipe(browserSync.reload({stream: true}))
);

task('watch', done => {
  console.log('Watch for:');
  console.group();
  for (let key in paths) {
    if (paths[key].hasOwnProperty('watch')) {
      let path = paths[key].watch;
      if (path === 'src') {
        path = paths[key].src
      }
      let watcher = watch(path, series(key));

      console.log('- ', path);

      watcher.on('all', function(stats, path) {
         console.log(`File ${path} was ${stats}`);
      });
    }
  }

  watch('www/*.html').on('change', browserSync.reload);

  console.groupEnd();
  done();
});

task('build', series('clean', parallel('scripts', 'styles', 'images', 'fonts')));

exports.default = parallel('watch', 'serve', 'scripts');
